from django.urls import path
from accounts.views import user_login, logout_, signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_, name="logout"),
    path("signup/", signup, name="signup"),
]
