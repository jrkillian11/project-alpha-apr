from django.urls import path
from tasks.views import create_task, assignee_list

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", assignee_list, name="show_my_tasks"),
]
