from django.contrib import admin
from tasks.models import Task

# Register your models here.
admin.site.register(Task)

# @admin.register(Task)
# class TaskAdmin(admin.ModelAdmin):
#    list_display = (
#        "id",
#        "start_date",
#        "due_date",
#        "is_completed",
#        "project",
#        "assignee",
#    )
